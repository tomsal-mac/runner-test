FROM node:18-bullseye-slim
WORKDIR /srv/
ADD setup.sh ./
RUN bash ./setup.sh
ADD hook.json /etc/webhook/hook.json
ADD action.js ./
RUN chmod +x action.js
EXPOSE 9000
ENTRYPOINT ["webhook"]
CMD ["-verbose", "-hooks=/etc/webhook/hook.json", "-hotreload"]

# FIXME don't run as root
