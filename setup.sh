#!/bin/bash
set -eux

webhookVersion=2.8.0
webhookSha=62ab801c7337a8b83de8d6ae8d7ace81

apt update
apt install -y wget
wget https://github.com/adnanh/webhook/releases/download/$webhookVersion/webhook-linux-amd64.tar.gz
echo "$webhookSha  webhook-linux-amd64.tar.gz" > sums.txt
md5sum -c sums.txt
tar xzf webhook-linux-amd64.tar.gz
rm webhook-linux-amd64.tar.gz sums.txt
find . -type f -name webhook -exec mv '{}' /usr/bin/ \;
webhook --version
apt remove -y wget
apt autoremove -y
rm -fr /var/lib/apt/lists
